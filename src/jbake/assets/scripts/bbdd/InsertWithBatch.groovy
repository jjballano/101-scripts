@Grab('mysql:mysql-connector-java:5.1.6')// <1>
@GrabConfig(systemClassLoader=true)
import Sql

def sql_orig = Sql.newInstance( "jdbc:mysql://localhost:3306/origen?jdbcCompliantTruncation=false", "user", "password", "com.mysql.jdbc.Driver")// <2>
def sql_dest =  Sql.newInstance( "jdbc:mysql://localhost:3306/destino?jdbcCompliantTruncation=false", "user", "password", "com.mysql.jdbc.Driver")

batchSize=20

sql_des.withBatch( batchSize, "insert into TABLE_DESTINO(a,b,c) values(?,?,?)'){ ps->   //<3>

   sql_orig.rows("select a,b,c from TABLE_ORIGEN").each{ row ->   //<4>

	row.a = row.a.toUpperCase().reverse()  //<5>

	ps.addBatch(row) //<6>
   }

}
//<7>
