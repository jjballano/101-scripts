@Grab(group='io.github.http-builder-ng', module='http-builder-ng-core', version='1.0.3') //<1>

import groovyx.net.http.*

String baseUrl = "http://servicios.ine.es"
String path = "/wstempus/js/ES/OPERACIONES_DISPONIBLES"

def httpBin = HttpBuilder.configure { //<2>
    request.uri = baseUrl
}

def operations = httpBin.get { //<3>
    request.uri.path = path
}

File ineOperations = new File('/tmp/ineOperations.txt') //<4>

operations.each {
    ineOperations << "${it}\n" //<5>
}