<!-- google -->
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

if (location.hostname === "localhost" || location.hostname === "127.0.0.1"){
    console.log("running on localhost")
}else {
    ga('create', 'UA-687332-11', 'auto');
    ga('send', 'pageview');
}

Lang.prototype.attrList.push('data-translate');
var lang = new Lang();
lang.init({
    defaultLang: 'es'
});

var pages = {
    'index':{
        expr:/index(\-)?([a-z]+)?\.html$/g,
        es: 'index.html',
        gb: 'index-en.html',
    },
    'about':{
        expr:/about(\-)?([a-z]+)?\.html$/g,
        es: 'about.html',
        gb: 'about-en.html',
    }
};

function checkLangPage(newLang){
    var currentLang = newLang || window.lang.currentLang;
    var url = $.parseUrl(document.location);
    if(url.pathname.endsWith('/'))
        url.pathname+='index.html';
    for(var p in pages ){
        var page = pages[p];
        var match=url.pathname.match(page.expr);
        if(match){
            if( page[currentLang] && !url.pathname.endsWith(page[currentLang])){
                window.location = '/101-scripts/'+page[currentLang];
                return true;
            }
        }
    }
    return false;
}

function changeLang(newLang) {
    lang.change(newLang);
    checkLangPage(newLang);
}


(function($) {

    $(document).ready(function() {
        checkLangPage();
    });

    $.extend({
        parseUrl: function(url) {
            var parser = document.createElement('a');
            parser.href = url;
            return parser;
        }
    });
})(jQuery);

