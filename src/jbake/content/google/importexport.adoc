= Import/Export de Google Sheet a Database
Jorge Aguilera <jorge.aguilera@puravida-software.com>
2017-12-30
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc, twitter, javafx, google, sheet
:jbake-category: google
:jbake-script: /scripts/google/ImportExportSheet.groovy
:idprefix:
:imagesdir: ../images


[abstract]
Para este post es necesario tener una cuenta en Google así como tener al menos una Hoja de Cálculo de Google Drive
(SpreadSheet).
Así mismo para poder acceder a las APIs de Google deberemos obtener unas credenciales que autorizen a la aplicación
a acceder a nuestra cuenta habilitandolo para ello desde la consola developer de Google.

A veces el departamento de informática tiene que dar soporte a otros departamentos en las tareas de exportar y/o
importar datos de la base de daos usando formatos que a otros departamentos les sea cómodo. Sin lugar a dudas, el formato
elegido en la mayoría de los casos es un fichero Excel de Microsoft. (Ver post en categoría *bbdd* y *office* para consultar
scripts ya documentados)

Sin embargo muchas veces este Excel debe ser revisado por más de una persona antes de volver a ser enviado al
departamento de informática para su importación en la base de datos por lo que se produce un riesgo de disparidad de
versiones en el fichero, infinidad de correos adjuntando la _última_ versión, etc.

En un mundo colaborativo como el de hoy en día, sería interesante si todas las partes (incluido el departamento de
informática) pudieran utilizar un único fichero de referencia siendo Google Sheet uno de los productos que lo permiten.

Así pues nuestro script va a consistir en, dado un SpreadSheet de Google y una cuenta de autorización:

- conectarse a la base de datos
- conectarse al SpreadSheet
- si la operación que se le indica es de exportar, recorrer todas las hojas del SpreadSheet y volcar sobre cada una
de ellas los datos de la tabla cuyo nombre coincida con la hoja
- si la operación es de importar, recorrer todas las hojas del SpreadSheet e importar todos las filas de la hoja
en la tabla cuyo nombre coincida con la hoja.


.Id del SpreadSheet
****
En este post vamos a utilizar un GroovyScript para conectarse a nuestro SpreadSheet de Google para leer y/o escribir
en el mismo por lo que necesitaremos saber su *id*. Este *id* se puede obtener de la propia URL que utiliza el navegador
cuando lo estamos editando. Por ejemplo, si la URL tiene este formato:

https://docs.google.com/spreadsheets/d/xxxxxxxxxtKhml999999999Icp123/edit#gid=0

Entonces *xxxxxxxxxtKhml999999999Icp123* es el *id* que necesitamos proporcionar como argumento al script.
****

== Consola Google

En primer lugar deberemos crear una aplicación en la consola de Google en https://console.developers.google.com

En esta aplicación habilitaremos (al menos) la API "Google Sheet API"

Así mismo deberemos crear unas credenciales de "Servicio" tras lo cual dispondremos de la posibilidad de descargar
un fichero JSON con las mismas y que deberemos ubicar junto con el script

== Groogle

Para facilitar la parte técnica de autentificación y creación de servicios he creado un proyecto de código abierto,
*Groogle*, disponible en https://gitlab.com/puravida-software/groogle el cual publica un artefacto en Bintray y que
podremos usar en nuestros scripts simplemente incluyendo su repositorio.
*Groogle* se compone de varios componentes:

- groogle-core, contiene la parte de autentificación y autorización del usuario así como proporcionar servicios básicos
- groogle-sheet, facilita la gestión de Sheets mediante un lenguaje específico (DSL) permitiendo la lectura y escritura
de una hoja de calculo
- groogle-drive, facilita la gestión de ficheros en Drive (aún está en desarrollo)
- groogle-calendar, facilita la gestión de Eventos de un Calendario mediante un lenguaje específico

== Dependencias

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=dependencies]
----

== Autorización

Groogle nos permitirá realizar el login de nuestra aplicación y guardar la autorización en nuestro disco de tal forma
que ejecuciones posteriores no requieran de volver a autorizar la aplicación (mientras no borremos el fichero con
la autorización generada)

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=login]
----
<1> client-secret.json es el fichero que hemos descargado de la consola de Google y que contiene las credenciales
<2> Groogle realiza el login y guarda la autorización en $HOME/.credentials/${applicationName}
<3> SheetScript ofrece un DSL para la gestión de lectura/escritura del SpreadSheet

== Database

Para la gestión de la base de datos hemos creado una serie de métodos simples:

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=database]
----
<1> Dado el nombre de una table, otener sus columnas mediante el metadata
<2> Borrado de una tabla. En nuestro caso y por ser MySQL hacemos un truncate
<3> Insertado optimizado de un List<List<Object>> (lista de lista de valores)
<4> Lectura de un lote de valores a partir de un registro dado

== Argumentos

Para la ejecución correcta del script necesitamos que nos proporcionen el *id* de la hoja así como la intención de
la ejecución, es decir, "De Google a Database" o "De Database a Google" lo cual lo representamos mediante los comandos
*g2d* o *d2g* respectivamente

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=cli]
----

Así por ejemplo una posible ejecución del scritp sería:

[source, console]
.De Google a Database
----
groovy ImportExportSheet.groovy -s xxxxxxxxxtKhml999999999Icp123 g2d
----

== Comparando Metadatos

En primer lugar, el script validará que los esquemas de la hoja y de la base de datos se corresponden, de tal forma
que ambos tienen las mismas columnas (para cada hoja de datos que contenga el SpreadSheet).

Para ello lee la primera fila de cada hoja y la compara con el metadata de la tabla con el mismo nombre en la base de datos.
Si no coinciden (por ejemplo la hoja contiene una columna más que la bbdd, o alguien ha cambiado el nombre de una columna) se aborta
el script mediante un _assert_

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=schemas]
----
<1> withSpreadSheet(id, closure), ejecuta la closure dentro de un contexto referenciado al SpreadSheet indicado
<2> el DSL nos permite acceder a todas las hojas mediante *sheets*
<3> withSheet(id, closure) ejecuta la closure dentro de un contexto referenciado a la hoja indicada
<4> readRows (rangoA, rangoB) realiza la lectura de un rango de celdas devolviendo un List<List<Object>>

== De Google a Database

Una vez validados los metadatas de cada hoja el script puede realizar la importanción desde las hojas a la base de datos
(si el argumento de la ejecución es *g2d*). Para ello recorrerá las filas de la hoja mediante *readRows* hasta que no
haya datos (cuando readRows devuelva _null_) y para evitar un número excesivo de lecturas lo hará en bloques de 100

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=g2d]
----

== De Database a Google

Una vez validados los metadatas de cada hoja el script puede realizar la importanción desde la base de datos a las
hojas (si el argumento de la ejecución es *d2g*). Para ello recorrerá los registros de la base de datos en bloques de
100 y los insertará en la hoja simplemente llamando el método *appendRows* que admite un List<List<Object>>

Previamente cada hoja es inicializada mediante *clearRange* añadiendo también los títulos mediante *appendRow* que
requiere un List<Object>

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=d2g]
----
