= Buscar el fichero de mayor tamaño recursivamente
Miguel Rueda <miguel.rueda.garcia@gmail.com>
2017-08-23
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc
:jbake-category: file
:jbake-script: /scripts/file/FindBiggerFile.groovy
:idprefix:
:imagesdir: ../images


A veces nos surge que necesitamos saber qué fichero está consumiendo más espacio pero debido a que tenemos
un número elevado de subdirectorios nos es dificil encontrarlo. En determinados sistemas operativos es
"fácil" encontrarlo concatenando varios comandos como _du_ _grep_ etc.

Mediante este script encontraremos la ruta del fichero con el mayor tamaño sin importar el número de subdirectorios

[source,groovy]
----
include::{sourcedir}{jbake-script}[]
----


A continuación pasamos a explicar de forma detallada el contenido de nuestro script de groovy que
cumple la función de encontrar el fichero de mayor tamaño:

Definiremos una función llamada `scanDir` encargada de recorrer de manera recursiva todos los directorios
y subdirectorios de la ruta indicada por parámetro. Comparará el tamaño de cada uno de los ficheros que van apareciendo
y guadará el mayor.

Para recorrer cada uno de los directorios y subdirectorios de la ruta indicada por parámetro utilizamos:

[source,groovy]
----
include::{sourcedir}{jbake-script}[lines=4..4]
----

Para recorrer los ficheros del directorio/subdirectorio encontrado utilizamos:

[source,groovy]
----
include::{sourcedir}{jbake-script}[lines=7..7]
----

Realizaremos la comparación entre el tamaño de los diferentes ficheros utilizando `f.size()`(este método nos devuelve
la dimensión del fichero):

[source,groovy]
----
include::{sourcedir}{jbake-script}[lines=5..5]
----

Para ejecutar este proceso  simplemente llamaremos a la función `scanDir` indicando la ruta que queremos revisar
(en este caso `.`):

[source,groovy]
----
include::{sourcedir}{jbake-script}[lines=12..12]
----

Al final del proceso obtendremos el fichero del cual podemos obtener la ruta al mismo, tamaño, etc

[source,groovy]
----
include::{sourcedir}{jbake-script}[lines=14..14]
----
