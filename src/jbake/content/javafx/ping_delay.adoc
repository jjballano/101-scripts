= Monitorea un servidor (o lo que quieras)
Jorge Aguilera <jorge.aguilera@puravida-software.com>
2017-10-23
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc, twitter, javafx
:jbake-category: javafx
:jbake-script: /scripts/javafx/PingDelay.groovy
:idprefix:
:imagesdir: ../images


.Qué es JavaFX
[quote, Wikipedia, https://es.wikipedia.org/wiki/JavaFX]
____
JavaFX es una familia de productos y tecnologías de Oracle Corporation (inicialmente Sun Microsystems),
para la creación de Rich Internet Applications (RIAs), esto es, aplicaciones web que tienen las características
y capacidades de aplicaciones de escritorio, incluyendo aplicaciones multimedia interactivas.

Las tecnologías incluidas bajo la denominación JavaFX son JavaFX Script y JavaFX Mobile, aunque
hay más productos JavaFX planeados
____


JavaFX puede verse como el sucesor a Swing en la medida en que nos permite crear aplicaciones gráficas aunque
en realidad pretende ocupar espacios donde este no llegó (lectores DVD por ejemplo). Cuenta además con un lenguaje
de scripting *Java FX Script* que permite desarrollar aplicaciones con menos código que con el stack tradicional Swing.
(Sí, podríamos decir que es la misma idea que Groovy y Groovy Script).

Y como era de esperar, Groovy cuenta con un proyecto que nos va a permitir hacernos la vida más fácil a la hora de
definir nuestros elementos gráficos así como conseguir que los cambios que se producen en el modelo se transmitan a la
vista de una forma realmente sencilla, el cual puedes encontrar en http://groovyfx.org/

== Caso de Uso

Para este ejemplo vamos a desarrollar un pequeño script que se va a conectar a un servidor web (parametrizable) y va
a ir monitorizando el tiempo de respuesta, de tal forma que si se encuentra caído en algún momento podamos verlo
rápidamente. Tanto si está caído como el tiempo que tarda en conectar lo vamos a mostrar en una ventana gráfica que
se irá refrescando automáticamente.

NOTE: Como el resto de scripts, este pretende ofrecerte las herramientas para que puedas ajustarlo a tus propias necesidades.
En este caso no es tan importante la funcionalidad sino el cómo.

Más o menos esto es lo que veremos en pantalla:

image::ping_delay.jpg[]

== Modelo-Vista-Controlador

No. Tranquilo. No vamos a desarrollar ningún sistema MVC complejo (aunque si te interesa el tema te recomiendo que
eches un ojo al proyecto http://griffon-framework.org). Simplemente vamos a separar dentro del mismo script la parte
correspondiente al _negocio_ de la parte correspondiente a la _vista_ para hacer nuestro script más legible.

Al realizar esta separación lo más importante es conseguir que los cambios que se producen en el negocio sean reflejados
en la vista (y viceversa aunque en este ejemplo no vamos a tratar esta dirección). Para ello JavaFX proporciona unas
clases similares a los Beans tanto en su funcionalidad como en su *verbosidad*, pero tranquilo que GroovyFx nos
permite reducirla.


Como puedes ver a continuación la clase Business simplemente consta de un String para guardar el server que queremos monitorizar y
un _FXBindable String_ el cual guarda el tiempo de respuesta a la última petición. Por lo demás simplemente un par de
métodos para calcular cuanto tarda una conexión a un puerto mediante sockets, nada del otro mundo (probablemente para
tu caso específico será aquí donde el problema no sea tan trivial):

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=business]
----
<1> FXBindable es propia del proyecto GroovyFx
<2> Al actualizar el valor como un String, los cambios se propagan


La segunda parte del script va a hacer uso del DSL que nos ofrece GroovyFX para poder diseñar los elementos gráficos (
stage, scene, label, inputs, buttons, etc) así como personalizar cada uno con los atributos que queramos, como tamaño,
posición, colores, texto etc.

Lo más importante (a parte de tener gusto para saber crear la parte gráfica) es unir nuestro modelo con la vista, lo
cual conseguimos mediante el método *bind*. Así en este script le indicamos a un label que su texto NO es un texto fijo
sino que está enlazado a una propiedad _delay_ del objecto _business_


[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=vista]
----
<1> vamos creando los elementos gráficos
<2> bind crea un listener a la propiedad de business y actualiza el elemento gráfico en consecuencia


== Transiciones

Mediante JavaFX/GroovyFX no sólo tenemos aplicaciones estáticas que reaccionan ante eventos del usuario sino que podemos
crear nuestras propias transiciones de una forma contínua, de tal forma que nuestra aplicación se puede ir refrescando
ella sóla.


[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=bucle]
----
<1> Creamos un bucle infinito de transiciones
<2> Al terminar cada transicion refrescamos nuestro negocio

