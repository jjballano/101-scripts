= Volcar datos de una tabla a Excel
Miguel Rueda <miguel.rueda.garcia@gmail.com>
2017-10-08
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc
:jbake-category: office
:jbake-script: /scripts/office/TableExcel.groovy
:idprefix:
:imagesdir: ../images



El caso práctico en el cuál este script nos puede resultar muy útil es cuando por ejemplo existe la necesidad
de extraer información de una base de datos para crear una serie de informes que nos solicita un cliente, como puede ser
obtener información de unos determinados productos.

Muchos motores de bases de datos y/o herramientas son capaces de exportar esta información a fichero CSV
(fichero delimitado por comas, punto y coma, pipes, etc) y posteriormente importarlo en un Excel.

Supongamos que disponemos de una tabla de productos donde entre otra información guardamos el código de producto y
la descripción.

|===
| SKU | DESCRIPTION

| 1 | LAPICES
| 2 | SACAPUNTAS
| XXXXX | UN PRODUCTO SUPERCOTIZADO

|===

Extraer esta información es trivial con una simple sentencia SQL:

----
select concat(sku,'|', description, '|') from products order by sku
----

y redirigir la salida de esta sentencia a un fichero para ser abierto con Excel.

Pero con este script nos evitamos todo el proceso de conversión, generando un Excel directamente:


[source,groovy]
----
include::{sourcedir}{jbake-script}[]
----
<1> Cargamos dependencias (MySQL y JExcel)
<2> Para cada registro que cumpla la query generamos una fila nueva
<3> Podemos generar cabeceras en la primera fila
<4> Ejemplo de cómo crear dos celdas de tipo texto, pero existen otros tipos como Date o Number
