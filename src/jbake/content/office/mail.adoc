= Mail
2017-08-04
:jbake-type: page
:jbake-status: draft
:jbake-tags: blog, asciidoc
:idprefix:

.Creamos la clase Mail.groovy

[source,console]
----
import javax.mail.*
import javax.mail.internet.*
import java.util.Properties;
import java.text.DecimalFormat;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


class Mail{
  def static sendEmail(subjetc,text,from,to){ // <1>

        def  d_email = from,
             d_uname = "",
             d_password = "",
             d_host = "smtp-relay.gmail.com",
             d_port  = "587", //465,587
             m_to = to,
             m_subject = subjetc,
             m_text = text

        def props = new Properties() // <2>
        props.put("mail.smtp.user", d_email)
        props.put("mail.smtp.host", d_host)
        props.put("mail.smtp.port", d_port)
        props.put("mail.smtp.starttls.enable","true")
        props.put("mail.smtp.debug", "false")
        props.put("mail.smtp.auth", "false")
        props.put("mail.protocol", "smtp")


        def session = Session.getInstance(props)
        session.setDebug(true);

        def msg = new MimeMessage(session)
        msg.setText(m_text)
        msg.setSubject(m_subject)
        msg.setFrom(new InternetAddress(d_email))
        msg.addRecipient(Message.RecipientType.TO, new InternetAddress(m_to))

        Transport.send( msg ) // <3>
    }
}
----
<1> Definimos el método `sendEmail(subjetc,text,from,to)` al cual mandamos como argumento el asunto, el texto que
que queremos incluir en el email, el emisor y el receptor.

<2> Definimos el `properties` que utilizaremos para enviar el email.

<3> Enviamos el email.

.Llamada y envío del email

Abrimos una consola de groovy en la misma posición en la que se encuentre nuesto Mail.groovy.
[source,console]
----
Mail.sendEmail("Email de test","Esto es una prueba de lo bien que lo hacemos",'emisor@gmail.com','receptor@gmail.com)
----
