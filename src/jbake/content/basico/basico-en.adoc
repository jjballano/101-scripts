= Basic concepts
Jorge Aguilera <jorge.aguilera@puravida-software.com>
2017-08-20
:jbake-lang: gb
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc
:jbake-category: basico
:jbake-script: /scripts/basico/Basico.groovy
:idprefix:
:imagesdir: ../images
:jbake-spanish: basico


Groovy is like Java, in fact you can use Java code and it will be valid en Groovy.
You can declare class, interfaces, etc but Groovy have some additional functionality and classes that are very useful
and less verbose than Java.

One of this additional functionality is the possibility to create a script files that can be execute directly without the compile step.
For example you can create utilities for system administrator.

In this post we are ready to create a basic script in which you can appreciate some groovy characteristic that make it special.

== Semicolon, parenthesis and more

First famous start Groovy topics is semicolon. Groovy not need ";" as end line, it is able to deduce it from the context and syntaxt.
But if you want you can put it.

When we use a single function on one line, Groovy not need the parentheses, in this way our code are much more readable.

[source,groovy]
----
include::{sourcedir}{jbake-script}[lines=1..5]
----
== Types declaration

Next step will be the type declaration or not (static vs dynamic).

Groovy allow both declaration types and you can declare functions and variables writing data type,
but if you use a dynamic declaration your code can be much moore readable and versatile.

When you are not interested in a variable data type (or function return) you can use *def*, in this way at executing time
Groovy look for the methods.

[source,groovy]
----
include::{sourcedir}{jbake-script}[lines=5..17]
----

== Single and double quotation marks

We can declare chains using single or double quotation marks. In this way we can include one of this into other.

If we would use a multi line String we use three double quotation mark to avoid escape character.

[source,groovy]
----
include::{sourcedir}{jbake-script}[lines=18..23]
----

== String vs GString

Groovy add a GString class that is like a String. When before we are using a doble chain we are instantiate a GString.

The added value of GString is that we can insert code into a chain that will be evaluate when the variable will be used,
in this way we can avoid typical java chain concatenations:

[source,groovy]
----
include::{sourcedir}{jbake-script}[lines=24..25]
----


== Closure

Before the new Java Lambda implementation, Groovy have the *closure* concept.

A closure is anonymous piece of code, that is assigned to a variable.
A closure can be have an input parameter and can be return a value like a functions.

We can invoke that closure with a implicit  *call(args)* method or send it as parameter.

[source,groovy]
----
include::{sourcedir}{jbake-script}[lines=26..32]
----

== List, mapa and so on

Declaring lists and maps, as well as assiging them is trivial

[source,groovy]
----
include::{sourcedir}{jbake-script}[lines=33..37]
----

== Loop

Besides the typical _for(int i=0; i<10;i++)_, Groovy offer "each" form in order to visit all list/map elements,
one of the most used:

[source,groovy]
----
include::{sourcedir}{jbake-script}[lines=38..40]
----

If you need know, in each moment, the position you can use "eachWithIndex"

[source,groovy]
----
include::{sourcedir}{jbake-script}[lines=41..43]
----
<1> The closure receives two parameters

